// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueGoodTable from 'vue-good-table'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import store from './vuex/Store'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import Snotify from 'vue-snotify'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.config.productionTip = false
Vue.use(VueGoodTable)
Vue.use(Snotify)
Vue.use(VueMomentJS, moment)
Vue.use(VueGoodTable)
Vue.use(Vuex)
Vue.use(VueResource)
Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
