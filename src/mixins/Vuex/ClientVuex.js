export default {
  computed: {
    allClients: function () {
      return this.$store.state.clientStore.clients
    }
  }
}
