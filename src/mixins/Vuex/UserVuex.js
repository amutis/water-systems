export default {
  computed: {
    allUsers: function () {
      return this.$store.state.userStore.all_users
    }
  }
}
