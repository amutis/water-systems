export default {
  computed: {
    allUserCategories: function () {
      return this.$store.state.userCategoryStore.all_user_categories
    }
  }
}
