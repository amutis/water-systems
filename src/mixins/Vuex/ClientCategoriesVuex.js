export default {
  computed: {
    allClientCategories: function () {
      return this.$store.state.clientCategoryStore.client_categories
    }
  }
}
