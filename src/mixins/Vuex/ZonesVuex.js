export default {
  computed: {
    allZones: function () {
      return this.$store.state.zoneStore.all_zones
    },
    zoneClients: function () {
      return this.$store.state.zoneStore.zones
    },
    zoneSingle: function () {
      return this.$store.state.zoneStore.zone
    }
  }
}
