import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Dashboard from '@/components/Module1/Dashboard'

// Bills
import PaidBills from '@/components/Module1/Bills/PaidBills'
import PendingBills from '@/components/Module1/Bills/PendingBills'
import ReconcileBill from '@/components/Module1/Bills/ReconcileBill'
import Bill from '@/components/Module1/Bills/Bill'

// Client
import AllClients from '@/components/Module1/Clients/AllClients'
import Client from '@/components/Module1/Clients/Client'
import ClientCategories from '@/components/Module1/Clients/ClientCategories'
import FlaggedClients from '@/components/Module1/Clients/FlaggedClients'
import RegisterClient from '@/components/Module1/Clients/RegisterClient'

// Meters
import ActiveMeters from '@/components/Module1/Meters/ActiveMeters'
import EnterReading from '@/components/Module1/Meters/EnterReading'
import InactiveMeters from '@/components/Module1/Meters/InactiveMeters'
import Meter from '@/components/Module1/Meters/Meter'
import MeterCategories from '@/components/Module1/Meters/MeterCategories'
import ReadingHistory from '@/components/Module1/Meters/ReadingHistory'

// Tasks
import AllTasks from '@/components/Module1/Tasks/AllTasks'
import CompleteTasks from '@/components/Module1/Tasks/CompleteTasks'
import NewTask from '@/components/Module1/Tasks/NewTask'
import OverDueTasks from '@/components/Module1/Tasks/OverDueTasks'
import Task from '@/components/Module1/Tasks/Task'
import TaskCategories from '@/components/Module1/Tasks/TaskCategories'

// Users
import AllUsers from '@/components/Module1/Users/AllUsers'
import DisabledUsers from '@/components/Module1/Users/DisabledUsers'
import RegisterUser from '@/components/Module1/Users/RegisterUser'
import User from '@/components/Module1/Users/User'
import UserCategories from '@/components/Module1/Users/UserCategories'

// Zones
import Zones from '@/components/Module1/Zones'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: Dashboard
        },
        // Bills
        {
          path: 'paid-bills',
          name: 'PaidBills',
          component: PaidBills
        },
        {
          path: 'pending-bills',
          name: 'PendingBills',
          component: PendingBills
        },
        {
          path: 'reconcile-bill',
          name: 'ReconcileBill',
          component: ReconcileBill
        },
        {
          path: 'bill/:bill_number',
          name: 'Bill',
          component: Bill
        },
        // Clients
        {
          path: 'all-clients',
          name: 'AllClients',
          component: AllClients
        },
        {
          path: 'client/:client_id',
          name: 'Client',
          component: Client
        },
        {
          path: 'flagged-clients',
          name: 'FlaggedClients',
          component: FlaggedClients
        },
        {
          path: 'register-client',
          name: 'RegisterClient',
          component: RegisterClient
        },
        {
          path: 'client-categories',
          name: 'ClientCategories',
          component: ClientCategories
        },
        // Meters
        {
          path: 'active-meters',
          name: 'ActiveMeters',
          component: ActiveMeters
        },
        {
          path: 'enter-reading',
          name: 'EnterReading',
          component: EnterReading
        },
        {
          path: 'inactive-meters',
          name: 'InactiveMeters',
          component: InactiveMeters
        },
        {
          path: 'meter/:meter_id',
          name: 'Meter',
          component: Meter
        },
        {
          path: 'meter-categories',
          name: 'MeterCategories',
          component: MeterCategories
        },
        {
          path: 'reading-history',
          name: 'ReadingHistory',
          component: ReadingHistory
        },
        // Tasks
        {
          path: 'all-tasks',
          name: 'AllTasks',
          component: AllTasks
        },
        {
          path: 'complete-tasks',
          name: 'CompleteTasks',
          component: CompleteTasks
        },
        {
          path: 'new-task',
          name: 'NewTask',
          component: NewTask
        },
        {
          path: 'overdue-tasks',
          name: 'OverDueTasks',
          component: OverDueTasks
        },
        {
          path: 'task/:task_id',
          name: 'Task',
          component: Task
        },
        {
          path: 'task-categories',
          name: 'TaskCategories',
          component: TaskCategories
        },
        // Users
        {
          path: 'all-users',
          name: 'AllUsers',
          component: AllUsers
        },
        {
          path: 'disabled-users',
          name: 'DisabledUsers',
          component: DisabledUsers
        },
        {
          path: 'register-user',
          name: 'RegisterUser',
          component: RegisterUser
        },
        {
          path: 'user/:user_id',
          name: 'User',
          component: User
        },
        {
          path: 'user-categories',
          name: 'UserCategories',
          component: UserCategories
        },
        // Zones
        {
          path: 'zones',
          name: 'Zones',
          component: Zones
        }
      ]
    }
  ],
  mode: 'history'
})
