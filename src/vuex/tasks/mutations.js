export default {
  GET_TASKS (state, data) {
    state.tasks = data.data
  },
  GET_ALL_TASKS (state, data) {
    state.all_tasks = data.data
  },
  GET_TASK (state, data) {
    state.task = data.data
  },
  GET_DELETED_TASKS (state, data) {
    state.deleted_tasks = data.data
  }
}
