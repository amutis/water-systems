import mutations from './mutations'
import actions from './actions'

const state = {
  all_tasks: [],
  tasks: [],
  task: [],
  deleted_tasks: []
}

export default {
  state, mutations, actions
}
