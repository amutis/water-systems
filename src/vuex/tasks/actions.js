import Vue from 'vue'
import {taskUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_tasks (context) {
    Vue.http.get(taskUrls.getAllTasks).then(function (response) {
      context.commit('GET_ALL_TASKS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_tasks (context, publicId) {
    Vue.http.get(taskUrls.getTasks + publicId).then(function (response) {
      context.commit('GET_TASKS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_tasks (context) {
    Vue.http.get(taskUrls.getDeletedTask).then(function (response) {
      context.commit('GET_DELETED_TASKS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_task (context, taskId) {
    Vue.http.get(taskUrls.getTask + taskId).then(function (response) {
      context.commit('GET_TASK', response.data)
      context.dispatch('loading_false')
    })
  },
  post_task (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(taskUrls.postTask, postData).then(function () {
      context.dispatch('get_all_tasks')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_task (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(taskUrls.postTask, postData).then(function () {
      context.dispatch('get_all_tasks')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_task (context, publicId) {
    Vue.http.get(taskUrls.deleteTask + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_TASKS', response.data)
      router.push({
        name: 'Module.DeletedTasks'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreTask (context, publicId) {
    Vue.http.get(taskUrls.restoreTask + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_TASKS', response.data)
      router.push({
        name: 'Module.Tasks'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
