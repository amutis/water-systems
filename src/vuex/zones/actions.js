import Vue from 'vue'
import {zoneUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_zones (context) {
    Vue.http.get(zoneUrls.getAllZones).then(function (response) {
      context.commit('GET_ALL_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_zones (context, publicId) {
    Vue.http.get(zoneUrls.getZones + publicId).then(function (response) {
      context.commit('GET_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_zones (context) {
    Vue.http.get(zoneUrls.getDeletedZone).then(function (response) {
      context.commit('GET_DELETED_ZONES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_zone (context, zoneId) {
    Vue.http.get(zoneUrls.getZone + zoneId).then(function (response) {
      context.commit('GET_ZONE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_zone (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(zoneUrls.postZone, postData).then(function () {
      context.dispatch('get_all_zones')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_zone (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(zoneUrls.postZone, postData).then(function () {
      context.dispatch('get_all_zones')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_zone (context, publicId) {
    Vue.http.get(zoneUrls.deleteZone + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_ZONES', response.data)
      router.push({
        name: 'Module.DeletedZones'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreZone (context, publicId) {
    Vue.http.get(zoneUrls.restoreZone + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ZONES', response.data)
      router.push({
        name: 'Module.Zones'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
