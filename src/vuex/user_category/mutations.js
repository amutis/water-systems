export default {
  GET_STARTS (state, data) {
    state.user_categories = data.data
  },
  GET_ALL_STARTS (state, data) {
    state.all_user_categories = data.data
  },
  GET_START (state, data) {
    state.user_category = data.data
  },
  GET_DELETED_STARTS (state, data) {
    state.deleted_user_categories = data.data
  }
}
