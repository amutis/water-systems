import mutations from './mutations'
import actions from './actions'

const state = {
  all_user_categories: [],
  user_categories: [],
  user_category: [],
  deleted_user_categories: []
}

export default {
  state, mutations, actions
}
