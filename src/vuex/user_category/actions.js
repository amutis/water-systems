import Vue from 'vue'
import {userCategoryUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_user_categories (context) {
    Vue.http.get(userCategoryUrls.getAllStarts).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_categories (context, publicId) {
    Vue.http.get(userCategoryUrls.getStarts + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_user_categories (context) {
    Vue.http.get(userCategoryUrls.getDeletedStart).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_category (context, publicId) {
    Vue.http.get(userCategoryUrls.getStart + publicId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  post_user_category (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(userCategoryUrls.postStart, postData).then(function () {
      context.dispatch('get_all_user_categories')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_user_category (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(userCategoryUrls.postStart, postData).then(function () {
      context.dispatch('get_all_user_categories')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_user_category (context, publicId) {
    Vue.http.get(userCategoryUrls.deleteStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedStarts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreStart (context, publicId) {
    Vue.http.get(userCategoryUrls.restoreStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.Starts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
