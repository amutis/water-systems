import Vue from 'vue'
import {userUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_users (context) {
    Vue.http.get(userUrls.getAllUsers).then(function (response) {
      context.commit('GET_ALL_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_users (context, publicId) {
    Vue.http.get(userUrls.getUsers + publicId).then(function (response) {
      context.commit('GET_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_users (context) {
    Vue.http.get(userUrls.getDeletedUser).then(function (response) {
      context.commit('GET_DELETED_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user (context, userId) {
    Vue.http.get(userUrls.getUser + userId).then(function (response) {
      context.commit('GET_USER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_user (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(userUrls.postUser, postData).then(function () {
      context.dispatch('get_all_users')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_user (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(userUrls.postUser, postData).then(function () {
      context.dispatch('get_all_users')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_user (context, publicId) {
    Vue.http.get(userUrls.deleteUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_USERS', response.data)
      router.push({
        name: 'Module.DeletedUsers'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreUser (context, publicId) {
    Vue.http.get(userUrls.restoreUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USERS', response.data)
      router.push({
        name: 'Module.Users'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
