import mutations from './mutations'
import actions from './actions'

const state = {
  all_users: [],
  users: [],
  user: [],
  deleted_users: []
}

export default {
  state, mutations, actions
}
