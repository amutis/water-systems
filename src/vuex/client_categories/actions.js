import Vue from 'vue'
import {clientCategoryUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_client_categories (context) {
    Vue.http.get(clientCategoryUrls.getAllClientCategories).then(function (response) {
      context.commit('GET_ALL_CLIENT_CATEGORIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_client_categories (context, publicId) {
    Vue.http.get(clientCategoryUrls.getClientCategories + publicId).then(function (response) {
      context.commit('GET_CLIENT_CATEGORIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_client_categories (context) {
    Vue.http.get(clientCategoryUrls.getDeletedClientCategory).then(function (response) {
      context.commit('GET_DELETED_CLIENT_CATEGORIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_client_category (context, publicId) {
    Vue.http.get(clientCategoryUrls.getClientCategory + publicId).then(function (response) {
      context.commit('GET_CLIENT_CATEGORY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_client_category (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(clientCategoryUrls.postClientCategory, postData).then(function () {
      context.dispatch('get_all_client_categories')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_client_category (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(clientCategoryUrls.postClientCategory, postData).then(function () {
      context.dispatch('get_all_client_categories')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_client_category (context, publicId) {
    Vue.http.get(clientCategoryUrls.deleteClientCategory + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CLIENT_CATEGORIES', response.data)
      router.push({
        name: 'Module.DeletedClientCategories'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreClientCategory (context, publicId) {
    Vue.http.get(clientCategoryUrls.restoreClientCategory + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CLIENT_CATEGORIES', response.data)
      router.push({
        name: 'Module.ClientCategories'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
