import mutations from './mutations'
import actions from './actions'

const state = {
  all_client_categories: [],
  client_categories: [],
  client_category: [],
  deleted_client_categories: []
}

export default {
  state, mutations, actions
}
