export default {
  GET_CLIENT_CATEGORIES (state, data) {
    state.client_categories = data.data
  },
  GET_ALL_CLIENT_CATEGORIES (state, data) {
    state.all_client_categories = data.data
  },
  GET_CLIENT_CATEGORY (state, data) {
    state.client_category = data.data
  },
  GET_DELETED_CLIENT_CATEGORIES (state, data) {
    state.deleted_client_categories = data.data
  }
}
