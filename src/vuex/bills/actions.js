import Vue from 'vue'
import {billUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_bills (context) {
    Vue.http.get(billUrls.getAllBills).then(function (response) {
      context.commit('GET_ALL_BILLS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_bills (context, publicId) {
    Vue.http.get(billUrls.getBills + publicId).then(function (response) {
      context.commit('GET_BILLS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_bills (context) {
    Vue.http.get(billUrls.getDeletedBill).then(function (response) {
      context.commit('GET_DELETED_BILLS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_bill (context, billId) {
    Vue.http.get(billUrls.getBill + billId).then(function (response) {
      context.commit('GET_BILL', response.data)
      context.dispatch('loading_false')
    })
  },
  post_bill (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(billUrls.postBill, postData).then(function () {
      context.dispatch('get_all_bills')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_bill (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(billUrls.postBill, postData).then(function () {
      context.dispatch('get_all_bills')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_bill (context, publicId) {
    Vue.http.get(billUrls.deleteBill + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_BILLS', response.data)
      router.push({
        name: 'Module.DeletedBills'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreBill (context, publicId) {
    Vue.http.get(billUrls.restoreBill + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_BILLS', response.data)
      router.push({
        name: 'Module.Bills'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
