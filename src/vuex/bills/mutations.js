export default {
  GET_BILLS (state, data) {
    state.bills = data.data
  },
  GET_ALL_BILLS (state, data) {
    state.all_bills = data.data
  },
  GET_BILL (state, data) {
    state.bill = data.data
  },
  GET_DELETED_BILLS (state, data) {
    state.deleted_bills = data.data
  }
}
