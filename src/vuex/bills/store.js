import mutations from './mutations'
import actions from './actions'

const state = {
  all_bills: [],
  bills: [],
  bill: [],
  deleted_bills: []
}

export default {
  state, mutations, actions
}
