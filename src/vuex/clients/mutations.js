export default {
  GET_CLIENTS (state, data) {
    state.clients = data.data
  },
  GET_ALL_CLIENTS (state, data) {
    state.all_clients = data.data
  },
  GET_CLIENT (state, data) {
    state.client = data.data
  },
  GET_DELETED_CLIENTS (state, data) {
    state.deleted_clients = data.data
  }
}
