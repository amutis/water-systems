import mutations from './mutations'
import actions from './actions'

const state = {
  all_clients: [],
  clients: [],
  client: [],
  deleted_clients: []
}

export default {
  state, mutations, actions
}
