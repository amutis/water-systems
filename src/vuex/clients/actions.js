import Vue from 'vue'
import {clientUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_clients (context) {
    Vue.http.get(clientUrls.getAllClients).then(function (response) {
      context.commit('GET_ALL_CLIENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_clients (context, publicId) {
    Vue.http.get(clientUrls.getClients + publicId).then(function (response) {
      context.commit('GET_CLIENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_clients (context) {
    Vue.http.get(clientUrls.getDeletedClient).then(function (response) {
      context.commit('GET_DELETED_CLIENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_client (context, clientId) {
    Vue.http.get(clientUrls.getClient + clientId).then(function (response) {
      context.commit('GET_CLIENT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_client (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(clientUrls.postClient, postData).then(function () {
      context.dispatch('get_all_clients')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_client (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(clientUrls.postClient, postData).then(function () {
      context.dispatch('get_all_clients')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_client (context, publicId) {
    Vue.http.get(clientUrls.deleteClient + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CLIENTS', response.data)
      router.push({
        name: 'Module.DeletedClients'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreClient (context, publicId) {
    Vue.http.get(clientUrls.restoreClient + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CLIENTS', response.data)
      router.push({
        name: 'Module.Clients'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
