import Vue from 'vue'
import Vuex from 'vuex'
import billStore from './bills/store'
import clientStore from './clients/store'
import taskStore from './tasks/store'
import meterStore from './meters/store'
import userStore from './users/store'
import zoneStore from './zones/store'
import clientCategoryStore from './client_categories/store'
import userCategoryStore from './user_category/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    billStore,
    clientStore,
    taskStore,
    userStore,
    zoneStore,
    meterStore,
    clientCategoryStore,
    userCategoryStore
  },
  strict: debug
})
