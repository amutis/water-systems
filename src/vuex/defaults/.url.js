const mainUrl = ''

export const clientCategoryUrls = {
  postClientCategory: mainUrl + 'starts/post',
  getClientCategories: mainUrl + 'starts',
  getClientCategory: mainUrl + 'starts/single/',
  deleteClientCategory: mainUrl + 'starts/delete/',
  editClientCategory: mainUrl + 'starts/edit/',
  getAllClientCategories: mainUrl + 'starts/all',
  restoreClientCategory: mainUrl + 'starts/restore/',
  getDeletedClientCategory: mainUrl + 'starts/deleted'
}

export const clientUrls = {
  postClient: mainUrl + 'starts/post',
  getClients: mainUrl + 'starts',
  getClient: mainUrl + 'starts/single/',
  deleteClient: mainUrl + 'starts/delete/',
  editClient: mainUrl + 'starts/edit/',
  getAllClients: mainUrl + 'starts/all',
  restoreClient: mainUrl + 'starts/restore/',
  getDeletedClient: mainUrl + 'starts/deleted'
}

export const billUrls = {
  postBill: mainUrl + 'starts/post',
  getBills: mainUrl + 'starts',
  getBill: mainUrl + 'starts/single/',
  deleteBill: mainUrl + 'starts/delete/',
  editBill: mainUrl + 'starts/edit/',
  getAllBills: mainUrl + 'starts/all',
  restoreBill: mainUrl + 'starts/restore/',
  getDeletedBill: mainUrl + 'starts/deleted'
}

export const taskUrls = {
  postTask: mainUrl + 'starts/post',
  getTasks: mainUrl + 'starts',
  getTask: mainUrl + 'starts/single/',
  deleteTask: mainUrl + 'starts/delete/',
  editTask: mainUrl + 'starts/edit/',
  getAllTasks: mainUrl + 'starts/all',
  restoreTask: mainUrl + 'starts/restore/',
  getDeletedTask: mainUrl + 'starts/deleted'
}

export const meterUrls = {
  postMeter: mainUrl + 'starts/post',
  getMeters: mainUrl + 'starts',
  getMeter: mainUrl + 'starts/single/',
  deleteMeter: mainUrl + 'starts/delete/',
  editMeter: mainUrl + 'starts/edit/',
  getAllMeters: mainUrl + 'starts/all',
  restoreMeter: mainUrl + 'starts/restore/',
  getDeletedMeter: mainUrl + 'starts/deleted'
}

export const zoneUrls = {
  postZone: mainUrl + 'starts/post',
  getZones: mainUrl + 'starts',
  getZone: mainUrl + 'starts/single/',
  deleteZone: mainUrl + 'starts/delete/',
  editZone: mainUrl + 'starts/edit/',
  getAllZones: mainUrl + 'starts/all',
  restoreZone: mainUrl + 'starts/restore/',
  getDeletedZone: mainUrl + 'starts/deleted'
}

export const userUrls = {
  postUser: mainUrl + 'starts/post',
  getUsers: mainUrl + 'starts',
  getUser: mainUrl + 'starts/single/',
  deleteUser: mainUrl + 'starts/delete/',
  editUser: mainUrl + 'starts/edit/',
  getAllUsers: mainUrl + 'starts/all',
  restoreUser: mainUrl + 'starts/restore/',
  getDeletedUser: mainUrl + 'starts/deleted'
}

export const userCategoryUrls = {
  postUserCategory: mainUrl + 'starts/post',
  getUserCategories: mainUrl + 'starts',
  getUserCategory: mainUrl + 'starts/single/',
  deleteUserCategory: mainUrl + 'starts/delete/',
  editUserCategory: mainUrl + 'starts/edit/',
  getAllUserCategories: mainUrl + 'starts/all',
  restoreUserCategory: mainUrl + 'starts/restore/',
  getDeletedUserCategory: mainUrl + 'starts/deleted'
}
