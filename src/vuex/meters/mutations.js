export default {
  GET_METERS (state, data) {
    state.meters = data.data
  },
  GET_ALL_METERS (state, data) {
    state.all_meters = data.data
  },
  GET_METER (state, data) {
    state.meter = data.data
  },
  GET_DELETED_METERS (state, data) {
    state.deleted_meters = data.data
  }
}
