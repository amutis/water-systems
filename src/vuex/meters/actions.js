import Vue from 'vue'
import {meterUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meters (context) {
    Vue.http.get(meterUrls.getAllMeters).then(function (response) {
      context.commit('GET_ALL_METERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meters (context, publicId) {
    Vue.http.get(meterUrls.getMeters + publicId).then(function (response) {
      context.commit('GET_METERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meters (context) {
    Vue.http.get(meterUrls.getDeletedMeter).then(function (response) {
      context.commit('GET_DELETED_METERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meter (context, meterId) {
    Vue.http.get(meterUrls.getMeter + meterId).then(function (response) {
      context.commit('GET_METER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meter (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(meterUrls.postMeter, postData).then(function () {
      context.dispatch('get_all_meters')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_meter (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(meterUrls.postMeter, postData).then(function () {
      context.dispatch('get_all_meters')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_meter (context, publicId) {
    Vue.http.get(meterUrls.deleteMeter + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_METERS', response.data)
      router.push({
        name: 'Module.DeletedMeters'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreMeter (context, publicId) {
    Vue.http.get(meterUrls.restoreMeter + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_METERS', response.data)
      router.push({
        name: 'Module.Meters'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
