import mutations from './mutations'
import actions from './actions'

const state = {
  all_meters: [],
  meters: [],
  meter: [],
  deleted_meters: []
}

export default {
  state, mutations, actions
}
